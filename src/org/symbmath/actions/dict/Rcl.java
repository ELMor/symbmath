package org.symbmath.actions.dict;

import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.rules.Identifier;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;
import org.symbmath.runtime.Exceptions.NotDefinedVar;
import org.symbmath.runtime.Exceptions.TooFewArgs;

public class Rcl implements Stackable {

	@Override
	public String getText() {
		return "rcl";
	}

	@Override
	public boolean mustRegister() {
		return true;
	}

	@Override
	public int argsNeeded() {
		return 1;
	}

	@Override
	public int returnNumber() {
		return 1;
	}

	@Override
	public boolean pushNoExec() {
		return false;
	}

	@Override
	public void exec(Ctx c) throws TooFewArgs {
		Identifier id=(Identifier)c.pop();
		Nodo n=(Nodo)c.dir.get(id.getText());
		if(n==null)
			throw new RuntimeException(new NotDefinedVar());
		c.push(n);
		
	}

}
