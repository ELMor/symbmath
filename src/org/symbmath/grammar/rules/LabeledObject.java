package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;

public class LabeledObject extends Nodo {
	/**
	 * 
	 */
	private static final long serialVersionUID = -632216312192597055L;
	public static final int label=0,objeto=1;
	
	public LabeledObject(){
		super(SymbMathParser.RULE_labeledObject);
	}
	
	public LabeledObject(Parser p, RuleContext pt) throws Exception  {
		super(pt.getRuleIndex());
		
		RuleContext l=gC(pt,1);
		RuleContext o=gC(pt,3);
		
		addChild(transform(p,l));
		addChild(transform(p,o));
	}
	
	@Override
	public String toString() {
		return ":"+g(label)+":"+g(objeto);
	}
}
