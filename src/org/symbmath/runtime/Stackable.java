package org.symbmath.runtime;

public interface Stackable {
	public int argsNeeded();
	public int returnNumber();
	public String getText();
	public boolean pushNoExec();
	public void exec(Ctx c) throws Exception;
	public boolean mustRegister();
}
