package org.symbmath.numerics;

import java.io.Serializable;

public class Ereal implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1333357386621768695L;
	public Real val;
	
	public int toInt(){
		Ereal r=new Ereal(this);
		return r.val.toInteger();
	}
	
	public Ereal(Ereal cop){
		val=new Real(cop.val);
	}
	
	public Ereal copy(){
		return new Ereal(this);
	}
	
	public Ereal(int v){
		val=new Real(v);
	}
	
	public Ereal(String s){
		val=new Real(s);
	}
	
	public Ereal(long v){
		val=new Real(v);
	}
	
	public Ereal plus(Ereal e2){
		Ereal r=new Ereal(this);
		r.val.add(e2.val);
		return r;
	}
	
	public Ereal plus(int e2){
		Ereal r=new Ereal(this);
		r.val.add(e2);
		return r;
	}
	
	public Ereal minus(Ereal e2){
		Ereal r=new Ereal(this);
		r.val.sub(e2.val);
		return r;
	}
	
	public Ereal minus(int e2){
		Ereal r=new Ereal(this);
		r.val.sub(e2);
		return r;
	}
	
	public Ereal mul(Ereal e2){
		Ereal r=new Ereal(this);
		r.val.mul(e2.val);
		return r;
	}
	
	public Ereal mul(int e2){
		Ereal r=new Ereal(this);
		r.val.mul(e2);
		return r;
	}
	
	public Ereal div(Ereal e2){
		Ereal r=new Ereal(this);
		r.val.div(e2.val);
		return r;
	}
	
	public Ereal div(int e2){
		Ereal r=new Ereal(this);
		r.val.div(e2);
		return r;
	}
	
	public Ereal sqr(){
		Ereal r=new Ereal(this);
		r.val.sqr();
		return r;
	}
	
	public Ereal sqrt(){
		Ereal r=new Ereal(this);
		r.val.sqrt();
		return r;
	}
	
	public Ereal pow(Ereal e2){
		Ereal r=new Ereal(this);
		r.val.pow(e2.val);
		return r;
	}
	
	public Ereal pow(int e2){
		Ereal r=new Ereal(this);
		r.val.pow(e2);
		return r;
	}
	
	public Ereal exp(){
		Ereal r=new Ereal(this);
		r.val.exp();
		return r;
	}
	
	public Ereal cos(){
		Ereal r=new Ereal(this);
		r.val.cos();
		return r;
	}
	
	public Ereal sin(){
		Ereal r=new Ereal(this);
		r.val.sin();
		return r;
	}
	
	public Ereal tan(){
		Ereal r=new Ereal(this);
		r.val.tan();
		return r;
	}
	
	public Ereal acos(){
		Ereal r=new Ereal(this);
		r.val.acos();
		return r;
	}
	
	public Ereal asin(){
		Ereal r=new Ereal(this);
		r.val.asin();
		return r;
	}
	
	public Ereal atan(){
		Ereal r=new Ereal(this);
		r.val.atan();
		return r;
	}
	
	public Ereal round(){
		Ereal r=new Ereal(this);
		r.val.round();
		return r;
	}
	
	public Ereal log(){
		Ereal r=new Ereal(this);
		r.val.log10();
		return r;
	}
	
	public Ereal ln(){
		Ereal r=new Ereal(this);
		r.val.ln();
		return r;
	}
	
	public Ereal exp10(){
		Ereal r=new Ereal(this);
		r.val.exp10();
		return r;
	}
	
	public Ereal hypot(Ereal e2){
		Ereal r=new Ereal(this);
		r.val.hypot(e2.val);
		return r;
	}
	
	public Ereal atan2(Ereal e2){
		Ereal r=new Ereal(this);
		r.val.atan2(e2.val);
		return r;
	}

	public Ereal mod(Ereal e2){
		Ereal r=new Ereal(this);
		r.val.mod(e2.val);
		return r;
	}

	public Ereal mod(int e2){
		Ereal r=new Ereal(this);
		r.val.mod(new Real(e2));
		return r;
	}

	public String toString(){
		return val.toString();
	}
	
	public boolean lt(Ereal e2){
		return val.lessThan(e2.val);
	}

	public boolean lt(int e2){
		return val.lessThan(e2);
	}

	public boolean gt(Ereal e2){
		return val.greaterThan(e2.val);
	}

	public boolean gt(int e2){
		return val.greaterThan(e2);
	}

	public boolean le(Ereal e2){
		return val.lessEqual(e2.val);
	}

	public boolean ge(Ereal e2){
		return val.greaterEqual(e2.val);
	}

	public boolean le(int e2){
		return val.lessEqual(e2);
	}

	public boolean ge(int e2){
		return val.greaterEqual(e2);
	}

	public boolean eq(Ereal e2){
		return val.equalTo(e2.val);
	}

	public boolean eq(int e){
		return val.equalTo(e);
	}

	public boolean isNegative(){
		return val.isNegative();
	}

	public boolean isZero(){
		return val.isZero();
	}

	public Ereal abs(){
		Ereal r=new Ereal(this);
		r.val.abs();
		return r;
	}
	
	public Ereal neg(){
		Ereal r=new Ereal(this);
		r.val.sign= (byte)(r.val.sign == 0 ? 1 : 0);
		return r;
	}
	
	private Ereal(Real arg){
		val=new Real(arg);
	}
	
	public Ereal min(Ereal e2){
		if(this.gt(e2))
			return e2;
		return this;
	}

	public Ereal min(int e2){
		Ereal d=new Ereal(e2);
		return min(d);
	}

	public Ereal max(Ereal e2){
		if(this.lt(e2))
			return e2;
		return this;
	}

	public Ereal max(int e2){
		Ereal d=new Ereal(e2);
		return max(d);
	}

	public Ereal or(Ereal e2){
		Ereal r=new Ereal(this);
		r.val.or(e2.val);
		return r;
	}
	
	public Ereal and(Ereal e2){
		Ereal r=new Ereal(this);
		r.val.and(e2.val);
		return r;
	}
	
	public Ereal xor(Ereal e2){
		Ereal r=new Ereal(this);
		r.val.xor(e2.val);
		return r;
	}
	
	public Ereal floor(){
		Ereal r=new Ereal(this);
		r.val.floor();
		return r;
	}
	
	public Ereal ceil(){
		Ereal r=new Ereal(this);
		r.val.ceil();
		return r;
	}
	
	public Ereal nroot(Ereal e2){
		Ereal r=new Ereal(this);
		r.val.nroot(e2.val);
		return r;
	}
	
	public Ereal inv(){
		Ereal r=new Ereal(this);
		r.val.recip();
		return r;
	}
	
	public boolean isIntegral(){
		return val.isIntegral();
	}
	
	public static final Ereal dos=new Ereal(2);
	public static final Ereal uno=new Ereal(1);
	public static final Ereal cero=new Ereal(0);
	public static final Ereal E=new Ereal(Real.E);
	public static final Ereal PI=new Ereal(Real.PI);
	public static final Ereal MAXR=new Ereal(Real.MAX);
	public static final Ereal MINR=new Ereal(Real.MIN);
	
}
