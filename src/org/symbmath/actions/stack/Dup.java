package org.symbmath.actions.stack;

import org.symbmath.grammar.Nodo;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;
import org.symbmath.runtime.Exceptions.TooFewArgs;

public class Dup implements Stackable {

	@Override
	public String getText() {
		return "dup";
	}

	@Override
	public boolean mustRegister() {
		return true;
	}

	@Override
	public int argsNeeded() {
		return 1;
	}

	@Override
	public int returnNumber() {
		return 1;
	}

	@Override
	public boolean pushNoExec() {
		return false;
	}

	@Override
	public void exec(Ctx c) throws TooFewArgs {
		Stackable p=c.pop();
		Nodo n=(Nodo)p;
		c.push((Nodo)n.clone());
		c.push((Nodo)n.clone());
	}

}
