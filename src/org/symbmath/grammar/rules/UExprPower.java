package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.numerics.Ereal;

public class UExprPower extends Nodo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7583254183563130973L;

	public UExprPower(){
		super(SymbMathParser.RULE_uExprPower);
	}
	
	public UExprPower(Parser p, RuleContext pt) {
		super(pt.getRuleIndex());
		
		ParseTree ptc=pt.getChild(0);
		Identifier id=new Identifier();
		id.setText(ptc.getText());

		int cc=pt.getChildCount();
		switch(cc){
		case 1: 
			mutate(id);
			break;
		default:
			Expresion ue=new Expresion();
			ue.subtype=OPExp.Pow;
			String tExpon=pt.getChild(cc-1).getText();
			NNumber expon=new NNumber(new Ereal(tExpon));
			if(cc==4)
				expon.chs(null);
			ue.addChild(id);
			ue.addChild(expon);
			mutate(ue);
		}
	}

	@Override
	public String toString() {
		Nodo id=getFirstChild();
		Nodo base=id.getNextSibling();		
		return id.getText() + (base==null?"":"^"+base.getText());
	}
	
}
