package org.symbmath.actions.arith;

import org.symbmath.grammar.rules.Expresion;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;

public class Colect implements Stackable {

	@Override
	public int argsNeeded() {
		return 1;
	}

	@Override
	public int returnNumber() {
		return 1;
	}

	@Override
	public String getText() {
		return "colect";
	}

	@Override
	public boolean pushNoExec() {
		return false;
	}

	@Override
	public void exec(Ctx c) throws Exception {
		Stackable p=c.stack.pop();
		if(p instanceof Expresion)
			c.push( ((Expresion)p).simplify() );
		else{
			c.push(p);
		}
	}

	@Override
	public boolean mustRegister() {
		return true;
	}

}
