package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;

public class Atom extends Nodo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4336459296411046513L;

	public Atom(){
		super(SymbMathParser.RULE_atom);
	}
/**
 * 	
atom 
	: nNumber
	| identifier
	| '(' expresion ')'
	| functionCall
	| derivative 
	| integral 
	| sumatoria 
	;

 */
	public Atom(Parser p, RuleContext pt) throws Exception {
		super(pt.getRuleIndex());
		RuleContext ch=null;
		switch(pt.getChildCount()){
		case 1: ch=gC(pt,0); break;
		case 3: ch=gC(pt,1); break; 
		}
		Nodo n=transform(p,ch);
		mutate(n);
	}

	@Override
	public String toString(){
		return g(0).toString();
	}
	
}
