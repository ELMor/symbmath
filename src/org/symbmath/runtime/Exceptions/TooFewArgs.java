package org.symbmath.runtime.Exceptions;

public class TooFewArgs extends Exception {
	public TooFewArgs(String p) {
		super(p);
	}

	public TooFewArgs() {
		super();
	}
}
