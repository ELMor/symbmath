package org.symbmath.tree;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import org.symbmath.grammar.InfixHelper;
import org.symbmath.grammar.InfixHelper.OPExp;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.rules.Expresion;
import org.symbmath.grammar.rules.FunctionCall;
import org.symbmath.grammar.rules.FunctionCallParams;
import org.symbmath.grammar.rules.Identifier;
import org.symbmath.grammar.rules.NNumber;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;
import org.symbmath.runtime.Exceptions.FunctionNotDefined;
import org.symbmath.runtime.Exceptions.TooFewArgs;

public class NBTree {
	boolean inv;
	Nodo ref;
	Vector<NBTree> hijos=new Vector<>();
	
	public NBTree(Nodo refer, boolean inverse) throws FunctionNotDefined, TooFewArgs{
		ref=(Nodo)refer.clone();
		inv=inverse;
		buildTree(refer,inv);
		canonical();
	}

	private NBTree(Nodo refer, boolean inverse, boolean dontBuildTree){
		ref=(Nodo)refer.clone();
		inv=inverse;
	}
	
	public void buildTree(Nodo e, boolean inv){
		if(e instanceof FunctionCall){
			NBTree function=add((Nodo)e.clone(),inv);
			//Nuevo nodo por cada parametro de la funcion
			for(Nodo param:e.g(1).childs)
				function.add(param,false).buildTree(param, false);
		}else if(e.size()<2){
			//Inv, Neg, NNumber, Identifier
			if(e instanceof NNumber || e instanceof Identifier){
				add((Nodo)e.clone(),inv);
			}else if(e.subtype==OPExp.Inv){
				add((Nodo)e.g(0).clone(),inv^false);
			}else if(e.subtype==OPExp.Neg){
				add((Nodo)e.g(0).clone(),inv^false);
			}
		}else{ //Infix operator
			if(ref.subtype==e.subtype){
				buildTree(e.g(0),inv^false);
				buildTree(e.g(1),inv^false);
			}else if(ref.subtype==e.reverse(e.subtype)){
				buildTree(e.g(0),inv^false);
				buildTree(e.g(1),inv^true);
			}else{
				add(e,inv).buildTree(e, false);
			}
		}
	}
	
	@SuppressWarnings("incomplete-switch")
	public void canonical() throws FunctionNotDefined, TooFewArgs{
		if(ref==null || ref.subtype==null)
			return;
		//De abajo a arriba
		for(NBTree h:hijos)
			h.canonical();
		//Handle x^y^z from ^(x y z) ==> ^(x *(y z))
		if(isPow() && hijos.size()>2){
			Ctx ctx=new Ctx(null,false);
			//Push Base
			fc().eval(ctx);
			ctx.push(NNumber.uno);
			for(int i=1;i<hijos.size();i++){
				hijos.get(i).eval(ctx);
				ctx.dynExec("*");
			}
			ctx.dynExec("^");
			popAndReplace(ctx);
			return;
		}
		switch(ref.subtype){
		case Res: changeInv(false); break;
		case Div: changeInv(true); break;
		}
		//match x inv x ==> remove
		if(isProduct() || isSum()){
			for(int i=0;i<hijos.size();i++)
				for(int j=i+1;j<hijos.size();j++)
					if(hijos.get(i).inverseOf(hijos.get(j))){
						replace(i,j,null);
						i--; //Reposition after deleting
						break;
					}
			if(canMutateSimplify())
				return;
		}
		//Match (x*y)^z ==> x^z * y^z
		if(isPow() && fc().isProduct()){
			Ctx ctx=new Ctx(null,false);
			ctx.push(NNumber.uno);
			for(int i=0;i<fc().size();i++){
				fc().hijos.get(i).eval(ctx);
				ctx.push(NNumber.uno);
				for(int j=1;j<hijos.size();j++){
					hijos.get(j).eval(ctx);
					ctx.dynExec("*");
				}
				ctx.dynExec("^");
				ctx.dynExec("*");
			}
			popAndReplace(ctx);
			return;
		}
		if(isProduct() || isSum())
			//Ordenar el vector de hijos
			Collections.sort(hijos, new Comparator<NBTree>() {
				@Override
				public int compare(NBTree o1, NBTree o2) {
					boolean o1inv=o1.inv;
					boolean o2inv=o2.inv;
					o1.inv=false;
					o2.inv=false;
					int ret=o1.toString().compareTo(o2.toString());
					o1.inv=o1inv;
					o2.inv=o2inv;
					return ret;
				}
			});
		//Collect exponents
		if(isProduct()){
			for(int i=0;i<hijos.size();i++){
				NBTree l=hijos.get(i);
				for(int j=i+1;j<hijos.size();j++){
					NBTree r=hijos.get(j);
					Nodo base=l.sameBase(r);
					if(base!=null){
						Expresion n=new Expresion(OPExp.Pow);
						n.addChild(base);
						n.addChild(l.addExpons(r));
						replace(i,j,n);
						i--;
						break;
					}
				}
			}
		}
		//Collect coefs of vars
		//Must handle x^2+x^2 ==> 2*x^2
		if(isSum()){
			for(int i=0;i<hijos.size();i++){
				NBTree l=hijos.get(i);
				for(int j=i+1;j<hijos.size();j++){
					NBTree r=hijos.get(j);
					Nodo vars=l.sameVarSet(r);
					if(vars!=null){
						Expresion n=new Expresion(OPExp.Mul);
						n.addChild(l.addCoefs(r));
						n.addChild(vars);
						replace(i,j,n);
						i--;
						break;
					}
				}
			}
		}
	}

	public Nodo addCoefs(NBTree o){
		Expresion e=new Expresion(OPExp.Sum);
		for(int i=0;i<hijos.size();i++)
			if( ! hijos.get(i).isId())
				e.addChild(hijos.get(i).ref);
		for(int i=0;i<o.hijos.size();i++)
			if( ! o.hijos.get(i).isId() )
				e.addChild(o.hijos.get(i).ref);
		switch(e.size()){
		case 0: return NNumber.dos;
		case 1: return (Nodo)e.g(0).clone();
		default: return e;
		}
	}
	
	
	public Nodo sameVarSet(NBTree o){
		Nodo st=getSummand();
		Nodo so=o.getSummand();
		if(st==null || so==null)
			return null;
		if(st.toString().equals(so.toString()))
			return st;
		return null;
	}
	/**Return a Node with the base if this is equal to 'o'
	 * Else, null
	 * @param o
	 * @return
	 */
	public Nodo sameBase(NBTree o){
		Nodo bt=getBase();
		Nodo ot=o.getBase();
		if(bt==null || ot==null)
			return null;
		return (Nodo)bt.clone();
	}
	
	public Nodo addExpons(NBTree o){
		Nodo base=sameBase(o);
		if(base==null)
			return null;
		Nodo suma=new Expresion(OPExp.Sum);
		suma.addChild(getExpon());
		suma.addChild(o.getExpon());
		return suma;
	}
	
	public Nodo getSummand(){
		if(isId())
			return ref;
		if(isProduct()){
			Expresion e=new Expresion(OPExp.Mul);
			for(int i=0;i<hijos.size();i++)
				if(hijos.get(i).isId())
					e.addChild((Nodo)hijos.get(i).ref.clone());
			switch(e.size()){
			case 0: return null;
			case 1: return e.g(0);
			default: return e;
			}
		}
		return null;
	}
	public Nodo getBase(){
		if(isId())
			return ref;
		if(isPow())
			return fc().ref;
		return null;
	}

	public Nodo getExpon(){
		if(isId())
			return NNumber.uno;
		if(isPow())
			return (Nodo)hijos.get(1).ref.clone();
		return null;
	}
	
	public void popAndReplace(Ctx ctx) throws TooFewArgs, FunctionNotDefined {
		ref=(Nodo) ctx.pop();
		hijos=new Vector<>();
		buildTree(ref,false);
		canonical();
	}

	private boolean canMutateSimplify(){
		if(hijos.size()==0){
			if(isProduct())
				ref=NNumber.uno;
			if(isSum())
				ref=NNumber.cero;
			return true;
		}
		if(hijos.size()==1){
			ref=fc().ref;
			inv=fc().inv;
			hijos=fc().hijos;
			return true;
		}
		return false;
	}
	
	private NBTree add(Nodo hijo, boolean inverse){
		NBTree ret=new NBTree(hijo,inverse,true);
		hijos.add(ret);
		return ret;
	}
	
	public void eval(Ctx ctx) throws  FunctionNotDefined, TooFewArgs{
		if(isNumber()){
			ctx.push(ref);
		}else if( isId() ){
			Stackable load=ctx.get(ref.getText());
			if(load==null)
				load=ref;
			ctx.push(ref);
		}else if( isFCall() ){
			String fname=this.ref.g(0).toString();
			Stackable function=ctx.get(fname);
			for(int i=0;i<hijos.size();i++)
				hijos.get(i).eval(ctx);
			if(function!=null){
				ctx.dynExec(fname);
			}else{
				Nodo id=new Identifier(fname);
				Nodo fcPars=new FunctionCallParams();
				for(int i=0;i<hijos.size();i++)
					fcPars.childs.insertElementAt((Nodo)ctx.pop(), 0);
				Nodo fCall=new FunctionCall();
				fCall.addChild(id);
				fCall.addChild(fcPars);
				ctx.push(fCall);
			}
		}else if(isPow()){
			fc().eval(ctx);
			sc().eval(ctx);
			ctx.dynExec("^");
		}else if(isProduct() || isSum()){
			boolean first=true;
			for(NBTree item:hijos){
				item.eval(ctx);
				if(first){
					first=false;
					if(item.inv){ //Problem if the first of the serie is inverted!
						if(isProduct()){
							ctx.push(NNumber.muno);
							ctx.dynExec("^");
						}
						if(isSum()){
							ctx.dynExec("CHS");
						}
					}
					continue;
				}
				String exec="^";
				if(isProduct()){
					exec=(item.inv ? "/":"*");
				}else if(isSum()){
					exec=(item.inv ? "-":"+");
				}
				try {
					ctx.dynExec(exec);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public String toString(){
		String ret = inv ? "inv(" :"";
		String par = inv ? ")" : "";
		if(ref.size()==0){
			ret+=ref.toString();
		}else{
			if(ref instanceof FunctionCall )
				ret+=ref.g(0).getText();
			else
				ret+=InfixHelper.subtName[ref.getSTNdx()];
			ret+="(";
			boolean f=true;
			for(NBTree hijo:hijos){
				if(f) f=false;
				else  ret+=" ";
				ret+=hijo.toString();
			}
			ret+=")";
		}
		return ret+par;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof NBTree){
			return this.toString().equals(obj.toString());
		}
		return false;
	}
	
	public boolean inverseOf(NBTree obj){
		inv=!inv;
		String th=this.toString();
		String ob=obj.toString();
		boolean ret=th.equals(ob);
		inv=!inv;
		return ret;
	}

	private void changeInv(boolean isDiv){
		ref.subtype=ref.reverse(ref.subtype);
		//Change inv of childs beggining at the second place
		for(int i=1;i<hijos.size();i++)
			hijos.get(i).inv=!hijos.get(i).inv;
	}
	
	public boolean isId(){
		return ref instanceof Identifier;
	}
	
	public boolean isNumber(){
		return ref instanceof NNumber;
	}
	
	public boolean isFCall(){
		return ref instanceof FunctionCall;
	}
	
	public boolean isPow(){
		if(ref==null || ref.subtype==null)
			return false;
		return ref.subtype==OPExp.Pow;
	}
	
	public boolean isProduct(){
		boolean ret;
		if(ref==null || ref.subtype==null)
			return false;
		switch(ref.subtype){
		case Mul: case Div: ret=true; break;
		default: ret=false;
		}
		return ret;
	}
	
	public boolean isSum(){
		boolean ret;
		if(ref==null || ref.subtype==null)
			return false;
		switch(ref.subtype){
		case Neg: case Sum: case Res: ret=true; break;
		default: ret=false;
		}
		return ret;
	}
	
	public int size(){
		return hijos.size();
	}
	
	private void replace(int uno, int dos, Nodo n){
		if(uno>dos){
			int k=uno;
			uno=dos;
			dos=k;
		}
		hijos.remove(dos);
		if(uno>-1)
			hijos.remove(uno);
		if(n!=null){
			if(uno<0)
				uno=dos;
			try {
				hijos.add(uno, new NBTree(n, false));
			} catch (FunctionNotDefined | TooFewArgs e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public NBTree fc(){
		if(hijos.size()>0)
			return hijos.get(0);
		return null;
	}

	public NBTree sc(){
		if(hijos.size()>1)
			return hijos.get(1);
		return null;
	}

}	
