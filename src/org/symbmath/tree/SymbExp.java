package org.symbmath.tree;

import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.rules.Expresion;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;
import org.symbmath.runtime.Exceptions.FunctionNotDefined;
import org.symbmath.runtime.Exceptions.TooFewArgs;

public class SymbExp {
	
	Expresion base;
	public NBTree nonBinRoot=null;
	public SymbExp(Expresion e) {
		base=e;
		try {
			nonBinRoot=new NBTree(e,false);
		} catch (FunctionNotDefined e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (TooFewArgs e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public String toString(){
		return nonBinRoot.toString();
	}
	
	public Nodo colect(){
		return toExp();
	}
	
	public Nodo expand(){
		return toExp();
	}
	
	public Nodo toExp() {
		Ctx ctx=new Ctx(null, false);
		try {
			nonBinRoot.eval(ctx);
		} catch (FunctionNotDefined | TooFewArgs e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Stackable res=null;
		try {
			res=ctx.pop();
		}catch(Exception tfa){
			tfa.printStackTrace();
		}
		return (Nodo)res;
	}
	
}
