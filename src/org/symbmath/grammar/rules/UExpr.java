package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;

public class UExpr extends Nodo {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2333764546862570317L;
	public static final int singleId=0,left=0,right=1;
	public UExpr(){
		super(SymbMathParser.RULE_uExpr);
	}
	
	public UExpr(Parser p, RuleContext pt)  throws Exception {
		super(pt.getRuleIndex());
		RuleContext chLeft=gC(pt,0),chRight=null;
		switch(pt.getChildCount()){
		case 1:
			mutate(transform(p,chLeft));
			break;
		case 3: 
			chRight=gC(pt,2); 
			addChild(transform(p,chLeft));
			addChild(transform(p,chRight));
			String ops=pt.getChild(1).getText();
			switch(ops.charAt(0)){
			case '^': subtype=OPExp.Pow; break;
			case '*': subtype=OPExp.Mul; break;
			case '/': subtype=OPExp.Div; break;
			}
			//Mutate to expresion
			type=SymbMathParser.RULE_expresion;
			break;
		}
	}

	@SuppressWarnings("incomplete-switch")
	@Override
	public String toString() {
		String ret=g(0).toString();
		if(childs.size()>1){
			switch(subtype){
			case Mul: ret+="*"; break;
			case Div: ret+="/"; break;
			case Pow: ret+="^"; break;
			}
			ret+=g(1);
		}
		return ret;
	}
}
