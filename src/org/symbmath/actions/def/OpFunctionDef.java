package org.symbmath.actions.def;

import org.symbmath.actions.DynExecAction;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.rules.Identifier;
import org.symbmath.grammar.rules.NoEvalExpresion;
import org.symbmath.grammar.rules.ParListIds;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;

public class OpFunctionDef  extends DynExecAction implements Stackable  {


	public Nodo exec(Ctx c, NoEvalExpresion e, ParListIds vars, Identifier id){
		vars.sibling=e;
		c.put(id.getText(), vars);
		return null;
	}

	@Override
	public int argsNeeded() {
		return 3;
	}

	@Override
	public int returnNumber() {
		return 0;
	}

	@Override
	public String getText() {
		return "$$DefineFunction";
	}
}
