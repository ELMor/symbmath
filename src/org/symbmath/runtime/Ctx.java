package org.symbmath.runtime;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;
import java.util.Vector;

import org.reflections.Reflections;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.rules.Magnitude;
import org.symbmath.runtime.Exceptions.FunctionNotDefined;
import org.symbmath.runtime.Exceptions.TooFewArgs;
import org.symbmath.units.UnitCatalog;

public class Ctx implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5653340291326144092L;
	static final String loadPackages[]={"org.symbmath"};
	static transient Hashtable<String, Stackable> system =new Hashtable<>(); 
	public static transient Hashtable<String, Magnitude> units=new Hashtable<>();
	static Vector<Hashtable<String, Stackable>> locals=new Vector<>();
	static {
		for(String p:loadPackages)
			registerStackablesAt(p);
	}
	public static void addUnit(String name, Magnitude m){
		if(units.get(name)!=null)
			System.err.println("Warning: Unit redefinition:"+name);
		units.put(name, m);
	}
	public Magnitude getUnit(String name){
		Magnitude ret=null;
		try {
			ret=(Magnitude)dir.get(name);
		}catch(Exception e){}
		if(ret==null)
			ret=units.get(name);
		return ret;
	}
	public static void registerStackablesAt(String pkgName) {
		//Load Units
		new UnitCatalog();
		//Load Functions
		Reflections reflections = new Reflections(pkgName);
		Set<Class<? extends Stackable>> subTypes = reflections.getSubTypesOf(Stackable.class);
		for(Iterator<Class<? extends Stackable>> it=subTypes.iterator();it.hasNext();){
			Class<? extends Stackable> c=it.next();
			try {
				Method m=c.getMethod("mustRegister", (Class<?>[])null);
				boolean register=(boolean) m.invoke(c.newInstance(), new Object[]{});
				if(register){
					try {
						Stackable exe = c.newInstance();
						system.put(exe.getText(), exe);
					} catch (Exception e2) {
						e2.printStackTrace();
					}
					
				}
			}catch(Exception e){
			}
		}
	}
	public static Stackable getSys(String name){
		return system.get(name);
	}
	public static void createLocal(){
		locals.add(new Hashtable<>());
	}
	public static void removeLocal(){
		if(locals.size()>0)
			locals.remove(locals.size()-1);
	}
	public static void putLocal(String name, Stackable exe){
		int s=locals.size()-1;
		if(s<0)
			throw new RuntimeException("No local environment opened!");
		locals.get(s).put(name, exe);
	}
	public Dir dir=new Dir(null,"/");
	public SStack stack=new SStack();
	public transient String fileNameSer=null;
	
	@SuppressWarnings("unchecked")
	public Ctx(String fname, boolean load) {
		fileNameSer=fname;
		if(!load)
			return;
		File f=new File(fname);
		if(f.exists()){ //try deserialize me
			try {
				FileInputStream fis=new FileInputStream(f);
				ObjectInputStream ois=new ObjectInputStream(fis);
				dir=(Dir)ois.readObject();
				stack=(SStack)ois.readObject();
				locals=(Vector<Hashtable<String,Stackable>>)ois.readObject();
				ois.close();
				System.out.println("Loaded session from "+f.getAbsolutePath());
			} catch (ClassNotFoundException | IOException e) {
				System.err.println("Error loading serialized session, ignoring");
			}
		}
	}
	
	public void quit() throws IOException{
		File f=new File(fileNameSer);
		FileOutputStream fos=new FileOutputStream(f);
		ObjectOutputStream oos=new ObjectOutputStream(fos);
		oos.writeObject(dir);
		oos.writeObject(stack);
		oos.writeObject(locals);
		oos.close();
		System.out.println("Saved session to "+f.getAbsolutePath());
	}
	public void dynExec(String fname) throws TooFewArgs, FunctionNotDefined {
		//Create instance of function
		Stackable fun=(Stackable) dir.get(fname);
		if(fun==null)
			throw new FunctionNotDefined(fname);
		
		int na=fun.argsNeeded();
		int pointer=stack.size()-na;
		
		//Enough args in stak?
		if(stack.size()<na)
			throw new TooFewArgs();
		
		//Inspect stack to find erguments types
		Class<?> typeArg[]=new Class<?>[na+1];
		typeArg[0]=Ctx.class;
		for(int i=0; i<na ;i++)
			typeArg[i+1]=stack.get(pointer+i).getClass();
		
		//search a method that matches that types of methods
		Method dynMethod= searchMethod(fun, typeArg);
		if(dynMethod==null){
			for(int i=0;i<na;i++){
				System.err.println("TypeArg#"+i+":"+stack.get(pointer+i).getClass());
			}
			throw new RuntimeException("Function is not defined for that type params.");
		}
		//collect values
		Object args[]=new Object[1+na];
		args[0]=Ctx.this;
		for(int i=0;i<na;i++)
			args[na-i]=stack.pop();
		//Push result into stack
		Stackable result=null;
		try {
			result = (Stackable)dynMethod.invoke(fun, args);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(result!=null){
			stack.superpush(result);
		}
	}
	public Method searchMethod(Stackable fun, Class<?>[] typeArg) {
		Method ret=null;
		//First match exact signature
		try { ret = fun.getClass().getMethod("exec",typeArg);
		} catch (NoSuchMethodException e) {}
		if(ret!=null)
			return ret;
		
		//Check other exec methods of the class.
		//Basically, Nodo is a placeholder for all types
		Method mts[]=fun.getClass().getMethods();
		for(Method m:mts){
			if(!m.getName().equals("exec"))
				continue; //Must be named 'exec'
			Class<?> pm[]=m.getParameterTypes();
			if(pm.length!=typeArg.length)
				continue; //Must have the same number of params
			//Check types, allowing Node for all
			boolean match=true;
			for(int i=0;i<pm.length;i++){
				if(pm[i].equals(Nodo.class))
					continue; //Enough, Nodo match all types
				if(pm[i].equals(typeArg[i]))
					continue; //Ok
				match=false;
			}
			if(match)
				return m;
		}
		//Finally, If there is a method 'void exec(Ctx c)', call it: unsafe!!
		/*
		Class<?> sing[]=new Class<?>[1];
		sing[0]=Nodo.class;
		try { ret = fun.getClass().getMethod("exec",sing);} 
		catch (NoSuchMethodException e) {}
		*/
		return ret;
	}
	@Override
	public String toString(){
		String ret=dir.toString()+"\n"+stack.toString();
		return ret;
	}
	public Stackable push(Stackable item){
		return stack.push(item);
	}
	public Stackable pop() throws TooFewArgs{
		try {
			return stack.pop();
		}catch(Exception r){
			throw new TooFewArgs();
		}
	}
	public void swap() throws TooFewArgs{
		if(stack.size()<2)
			throw new TooFewArgs("swap");
		Stackable p1=pop();
		Stackable p2=pop();
		push(p1);
		push(p2);
	}
	public Stackable get(String name){
		return dir.get(name);
	}
	public void put(String name, Stackable n){
		dir.put(name, n);
	}
	public class SStack extends Stack<Stackable> implements Serializable{

		private static final long serialVersionUID = 662557272589584614L;
		
		public Stackable superpush(Stackable item){
			return super.push(item);
		}
		
		@Override
		public Stackable push(Stackable item) {
			try {
				if(item.pushNoExec()) super.push(item);
				else dynExec(item.getText());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			return item;
		}
		
		public void push(Vector<Nodo> items){
			for(Stackable item:items)
				push(item);
		}
		
		@Override
		public synchronized String toString() {
			String ret="";
			if(size()>0) {
				int level=1;
				for(int i=size()-1; i>=0; i--){
					ret=String.format("%02d: ", level++)+
							String.format("[%s] ", get(i).getClass().getSimpleName().substring(0,3))+
							elementAt(i).toString()+
							"\n"+ret;
				}
			}else{
				ret="<Empty Stack>";
			}
			return ret;
		}
	}
	public class Dir implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 6639604993734582194L;

		public String getPath(){
			return (padre==null?"":padre.getPath())+"/"+name;
		}
		
		/**
		 * Load all classes extending Executable found at pkgName package 
		 * @param pkgName
		 */
		public String name;
		public Dir padre;
		Hashtable<String, Stackable> content=new Hashtable<>();
		HashMap<String,Dir> subdirs=new HashMap<>();
		
		public Dir(Dir parent, String n){
			padre=parent;
			name=n;
		}
		
		public Dir mkdir(String n){
			Dir ret=new Dir(this,n);
			subdirs.put(n,ret);
			return ret;
		}
		
		public void rmdir(String n){
			subdirs.remove(n);
		}
		
		public Dir updir(){
			return padre;
		}
		
		public Dir chdir(String name){
			return subdirs.get(name);
		}
		
		public void put(String name, Stackable exe){
			content.put(name, exe);
		}
		
		public void purge(String name){
			content.remove(name);
		}
		
		public Stackable get(String name){
			if(name==null)
				name=null;
			Stackable ret=null;
			int szl=locals.size()-1;
			while( ret==null && szl>=0 ){
				ret=locals.get(szl).get(name);
				szl--;
			}
			if(ret==null)
				ret=content.get(name);
			Dir p=padre;
			while( ret==null && p!=null ){
				ret=padre.content.get(name);
				p=p.padre;
			}
			if(ret==null)
				ret=system.get(name);
			return ret;
		}
		
		public String toString(){
			String ret="";
			if(locals.size()>0) {
				ret+="Local Environments ("+locals.size()+")\n";
				for(int i=0;i<locals.size();i++){
					Hashtable<String, Stackable> l=locals.get(i);
					ret+="("+(i+1)+")\n";
					for(String s:l.keySet()){
						ret+="  "+s+"="+l.get(s)+"\n";
					}
				}
			}
			if(content.size()+subdirs.size()>0){
				ret+="Userdir:"+getPath();
				for(String s:content.keySet()){
					ret+=s+"="+content.get(s)+";";
				}
				for(Dir subdir:subdirs.values()){
					ret+="\n"+subdir.toString();
				}
			}
			/*
			ret+="\nSystem Objects:";
			for(String s:system.keySet())
				ret+=s+" ";
			*/
			return ret;
		}	
	}
	public void debug(){
		System.out.println(toString());
	}
}
