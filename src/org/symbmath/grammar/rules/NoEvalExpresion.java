package org.symbmath.grammar.rules;

import java.util.Vector;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;

public class NoEvalExpresion extends Nodo implements Stackable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7538309585738102434L;

	public NoEvalExpresion(){
		super(SymbMathParser.RULE_noEvalExpresion);
	}
	
	public NoEvalExpresion(Parser p, RuleContext pt)  throws Exception {
		super(pt.getRuleIndex());
		addChild(transform(p,gC(pt,1)));
	}

	@Override
	public String toString(){
		return "'"+g(0).toString()+"'";
	}
	
	@Override
	public int argsNeeded() {
		return 0;
	}

	@Override
	public int returnNumber() {
		return 1;
	}
	
	@Override
	public void exec(Ctx c) {
		c.push(this);
	}
}
