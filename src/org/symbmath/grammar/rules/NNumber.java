package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.numerics.Ereal;
import org.symbmath.runtime.Ctx;

public class NNumber extends Nodo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7668083045145986699L;
	private Ereal rPart=null;
	private Ereal iPart=null;
	
	public static final NNumber cero=new NNumber(new Ereal(0));
	public static final NNumber uno =new NNumber(new Ereal(1));
	public static final NNumber muno=new NNumber(new Ereal(-1));
	public static final NNumber dos =new NNumber(new Ereal(2));
	
	@Override
	public boolean equals(Object obj) {
		return toString().equals(obj.toString());
	}

	public NNumber(){
		super(SymbMathParser.RULE_nNumber);
	}
	
	private void parseFromText(){
		NNumber parsed;
		try {
			parsed = (NNumber)Nodo.parse(SymbMathParser.RULE_nNumber, getText());
			rPart=parsed.rPart;
			iPart=parsed.iPart;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public NNumber(Parser p, RuleContext pt) {
		super(pt.getRuleIndex());
		switch(pt.getChildCount()){
		case 1: 
			setText(pt.getChild(0).getText());
			rPart=new Ereal(getText());
			break;
		case 5:
			rPart=new Ereal(pt.getChild(1).getText());
			iPart=new Ereal(pt.getChild(3).getText());
			setText("("+rPart.toString()+";"+iPart.toString()+")");
			break;
		}
	}

	public NNumber chs(Ctx c){
		if(rPart==null)
			parseFromText();
		if(iPart==null)
			return new NNumber(rPart.neg());
		return new NNumber(rPart.neg(),iPart.neg());
	}
		
	public String toString(){
		return getText();
	}
	public NNumber(int i){
		this(new Ereal(i));
	}
	public NNumber(Ereal v){
		this();
		setText(v.toString());
		rPart=v;
	}
	public NNumber(Ereal v, Ereal i){
		this();
		rPart=new Ereal(v);
		iPart=new Ereal(i);
		setText("("+rPart.toString()+";"+iPart.toString()+")");
	}

	@Override
	public void exec(Ctx c) {
		if(rPart==null)
			parseFromText();
		c.push(this);
	}
	
	public NNumber sum(Ctx c, NNumber r) {
		Ereal r1=new Ereal(getText());
		Ereal r2=new Ereal(r.getText());
		Ereal res=r1.plus(r2);
		return new NNumber(res);
	}
	
	public NNumber res(Ctx c, NNumber r) {
		Ereal r1=new Ereal(getText());
		Ereal r2=new Ereal(r.getText());
		Ereal res=r1.minus(r2);
		return new NNumber(res);
	}
	
	public NNumber mul(Ctx c, NNumber r) {
		Ereal r1=new Ereal(getText());
		Ereal r2=new Ereal(r.getText());
		Ereal res=r1.mul(r2);
		return new NNumber(res);
	}
	
	public NNumber div(Ctx c, NNumber r) {
		Ereal r1=new Ereal(getText());
		Ereal r2=new Ereal(r.getText());
		Ereal res=r1.div(r2);
		return new NNumber(res);
	}

	public NNumber pow(Ctx c,NNumber r) throws Exception{
		Ereal r1=new Ereal(getText());
		Ereal r2=new Ereal(r.getText());
		Ereal res=r1.pow(r2);
		return new NNumber(res);
	}

	public static boolean both(Nodo n1, Nodo n2){
		if(n1 instanceof NNumber && n2 instanceof NNumber)
			return true;
		return false;
	}
	@Override
	public String showTree() {
		return toString();
	}
}
