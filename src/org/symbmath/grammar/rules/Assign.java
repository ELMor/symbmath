package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Exceptions.FunctionNotDefined;
import org.symbmath.runtime.Exceptions.TooFewArgs;

public class Assign extends Nodo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8302910186393512125L;

	public Assign(){
		super(SymbMathParser.RULE_objeto);
	}
	
	public Assign(Parser p, RuleContext pt)  throws Exception {
		super(pt.getRuleIndex());
		addChild(transform(p,gC(pt,0)));
		addChild(transform(p,gC(pt,2)));
	}

	@Override
	public String toString(){
		return g(0).toString()+"="+g(1).toString();
	}
	
	@Override
	public void exec(Ctx c) throws TooFewArgs, FunctionNotDefined {
		c.push(g(1));
		c.push(g(0));
		c.dynExec("$$DefineVariable");
	}
}
