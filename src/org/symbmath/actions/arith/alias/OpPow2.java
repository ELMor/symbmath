package org.symbmath.actions.arith.alias;

import org.symbmath.actions.arith.OpPow;

public class OpPow2 extends OpPow {
	@Override
	public String getText() {
		return "pow";
	}
}
