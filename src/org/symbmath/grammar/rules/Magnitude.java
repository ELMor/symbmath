package org.symbmath.grammar.rules;

import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.SymbMathParser;
import org.symbmath.numerics.Ereal;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Exceptions.InconsistenUnitsException;
import org.symbmath.runtime.Exceptions.UndefinedException;
import org.symbmath.tree.SymbExp;
import org.symbmath.units.SIUnit;

public class Magnitude extends Nodo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2604882388740372981L;
	public static final int val=0,unit=1;
	
	public Magnitude(){
		super(SymbMathParser.RULE_magnitude);
	}
	
	public Magnitude(Parser p, RuleContext pt)  throws Exception {
		super(pt.getRuleIndex());
		addChild(new NNumber(new Ereal(pt.getChild(0).getText())));
		RuleContext m= gC(pt,2);
		addChild(transform(p,m));
	}
	public Magnitude(NNumber n, Nodo m){
		this();
		addChild(n);
		if(m instanceof  Expresion)
			addChild( new SymbExp((Expresion)m).colect()); 
		else
			addChild(m);
	}
	@Override
	public String toString() {
		return g(val)+"_"+g(unit);
	}
	
	@Override
	public void exec(Ctx c) throws Exception {
		c.push(this);
	}
	public NNumber valor(){
		return new NNumber(new Ereal(g(0).getText()));
	}
	public void setValor(NNumber n){
		childs.set(0, n);
	}
	public Nodo unit(){
		return g(1);
	}
	/**
	 * 
	 * @return
	 * @throws UndefinedException 
	 */
	public Magnitude ubase(Ctx c) throws UndefinedException{
		Magnitude unar=new Magnitude(NNumber.uno,this.unit());
		Magnitude base=unar.recUbase(c);
		NNumber retVal=this.valor().mul(c, base.valor());
		return new Magnitude(retVal,base.unit());
	}
	
	@SuppressWarnings("incomplete-switch")
	public Magnitude recUbase(Ctx c) throws UndefinedException {
		Nodo thisUnit=unit();
		NNumber thisVal=valor();
		switch(thisUnit.childs.size()){
		case 0: //es un Identifier
			Magnitude id=c.getUnit(thisUnit.getText());
			if(id==null)
				throw new UndefinedException();
			if(id instanceof SIUnit){
				return new Magnitude(thisVal,thisUnit);
			}else{
				NNumber vret=thisVal.mul(c, id.valor());
				return new Magnitude(vret,id.unit()).ubase(c);
			}
		case 2: //Es una operacion
			Magnitude left=new Magnitude(NNumber.uno,thisUnit.g(0)).ubase(c);
			Magnitude ret=null;
			switch(thisUnit.subtype){
			case Mul: 
				Magnitude rm=new Magnitude(NNumber.uno,thisUnit.g(1)).ubase(c);
				ret=left.mul(c, rm);
				break;
			case Div: 
				Magnitude rd=new Magnitude(NNumber.uno,thisUnit.g(1)).ubase(c);
				ret=left.div(c, rd);
				break;
			case Pow:
				NNumber rn=(NNumber)thisUnit.g(1);
				ret=left.pow(c,rn);
				break;
			}
			return ret;
		}
		return null;
	}
	/**
	 * Convert this unit
	 * @param c Context
	 * @param to The other unit to convert
	 * @return Converted unit
	 * @throws InconsistenUnitsException
	 * @throws UndefinedException
	 */
	public Magnitude convert(Ctx c, Magnitude to) throws InconsistenUnitsException, UndefinedException {
		Magnitude thisBase=ubase(c);
		Magnitude toBase=new Magnitude(NNumber.uno,to.g(1)).ubase(c);
		if(thisBase.g(1).equals(toBase.g(1))){			
			NNumber retVal=thisBase.valor().div(c, toBase.valor());
			return new Magnitude(retVal,to.unit());
		}
		throw new InconsistenUnitsException();
	}
	/**
	 * Divide this unit by another
	 * @param c Context
	 * @param r Dividend
	 * @return division
	 */
	public Magnitude div(Ctx c, Magnitude r){
		Ereal num=new Ereal(g(0).getText());
		Ereal den=new Ereal(r.g(0).getText());
		NNumber resn=new NNumber(num.div(den));
		Expresion resu=new Expresion();
		resu.subtype=OPExp.Div;
		resu.addChild(unit());
		resu.addChild(r.unit());
		return new Magnitude(resn,resu);
	}
	/**
	 * Multiply this unit by other
	 * @param c Context
	 * @param r the unit to multiply by
	 * @return The product
	 */
	public Magnitude mul(Ctx c, Magnitude r){
		Ereal num=new Ereal(g(0).getText());
		Ereal den=new Ereal(r.g(0).getText());
		NNumber resn=new NNumber(num.mul(den));
		Expresion resu=new Expresion();
		resu.subtype=OPExp.Mul;
		resu.addChild(unit());
		resu.addChild(r.unit());
		return new Magnitude(resn,resu);
	}
	/**
	 * Raise this unit to a power
	 * @param c Context
	 * @param r Number to raise
	 * @return 
	 */
	public Magnitude pow(Ctx c, NNumber r){
		Ereal resn=new Ereal(getSN()).pow(new Ereal(r.getText()));
		Expresion resu=new Expresion();
		resu.subtype=OPExp.Pow;
		resu.addChild(unit());
		resu.addChild(r);
		return new Magnitude(new NNumber(resn),resu);
	}	
	/**
	 * Factorize unit
	 * @param c 
	 * @param to Unit to factoize
	 * @return 
	 * @throws UndefinedException
	 */
	public Magnitude ufact(Ctx c, Magnitude to) throws UndefinedException {
		Magnitude thisUbase=ubase(c);
		to.setValor(NNumber.uno);
		Magnitude toUbase=to.ubase(c);
		Magnitude div=thisUbase.div(c, toUbase);
		Magnitude au=new Magnitude(NNumber.uno,to.unit());
		Magnitude prod=div.mul(c,au);
		return prod;
	}
	/**
	 * Sum a magnitude to this one 
	 * @param c Context
	 * @param r summand
	 * @return
	 * @throws Exception if not of the same basic units
	 */
	public Magnitude sum(Ctx c, Magnitude r) throws Exception{
		Magnitude rc=r.convert(c, this);
		Ereal sum=new Ereal(getSN()).plus(new Ereal(rc.getSN()));
		return new Magnitude(new NNumber(sum),unit());
	}
	/**
	 * Return real part of the value as String
	 * @return
	 */
	public String getSN(){
		return valor().toString();
	}
	/**
	 * Change sign
	 * @param c Context 
	 * @param eo 
	 * @return
	 * @throws Exception
	 */
	public Magnitude chs(Ctx c) throws Exception {
		Magnitude ret=new Magnitude();
		ret.mutate(this);
		ret.valor().chs(c);
		return ret;
	}
	public Magnitude mul(Ctx c,  NNumber r){
		Ereal p1=new Ereal(getSN());
		Ereal p2=new Ereal(r.getText());
		return new Magnitude(new NNumber(p1.mul(p2)),unit().simplify());
	}

	public Magnitude div(Ctx c, NNumber r) {
		Ereal p1=new Ereal(getSN());
		Ereal p2=new Ereal(r.getText());
		return new Magnitude(new NNumber(p1.div(p2)),unit().simplify());
	}
	public Nodo res(Ctx c, Magnitude r) throws Exception{
		Magnitude rc=r.convert(c, this);
		Ereal sum=new Ereal(getSN()).minus(new Ereal(rc.getSN()));
		return new Magnitude(new NNumber(sum),unit().simplify());
	}

	public String showTree(){
		return g(0).toString()+"_"+g(1).showTree();
	}
}
