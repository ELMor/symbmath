package org.symbmath.actions.units;

import org.symbmath.grammar.rules.Magnitude;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;
import org.symbmath.runtime.Exceptions.InconsistenUnitsException;
import org.symbmath.runtime.Exceptions.UndefinedException;

public class Ubase implements Stackable {

	@Override
	public int argsNeeded() {
		return 1;
	}

	@Override
	public int returnNumber() {
		return 1;
	}

	@Override
	public String getText() {
		return "ubase";
	}

	@Override
	public boolean pushNoExec() {
		return false;
	}

	@Override
	public void exec(Ctx c) throws Exception {
		c.dynExec(getText());
	}

	@Override
	public boolean mustRegister() {
		return true;
	}

	public Magnitude exec(Ctx c, Magnitude from) throws InconsistenUnitsException, UndefinedException{
		return from.ubase(c);
	}
	
}
