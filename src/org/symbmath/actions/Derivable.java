package org.symbmath.actions;

import org.symbmath.grammar.Nodo;

public interface Derivable {
	public Nodo derive(Nodo expresion, Nodo listVar);
}
