package org.symbmath.actions.debug;

import org.symbmath.grammar.Nodo;
import org.symbmath.grammar.rules.Expresion;
import org.symbmath.runtime.Ctx;
import org.symbmath.runtime.Stackable;
import org.symbmath.tree.SymbExp;

public class Hojas implements Stackable {

	@Override
	public int argsNeeded() {
		return 1;
	}

	@Override
	public int returnNumber() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return "hojas";
	}

	@Override
	public boolean pushNoExec() {
		return false;
	}

	public Nodo exec(Ctx c, Nodo n) throws Exception {
		if(n instanceof Expresion){
			SymbExp s=new SymbExp((Expresion)n);
			//System.out.println( s.nonBinRoot.hojas() );
		}
		return n;
	}

	@Override
	public boolean mustRegister() {
		return true;
	}

	@Override
	public void exec(Ctx c) throws Exception {
		c.dynExec(getText());
	}

}
