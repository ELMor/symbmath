// Generated from SymbMath.g4 by ANTLR 4.5.2

package org.symbmath.grammar;
import org.symbmath.grammar.Nodo;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SymbMathParser}.
 */
public interface SymbMathListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#console}.
	 * @param ctx the parse tree
	 */
	void enterConsole(SymbMathParser.ConsoleContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#console}.
	 * @param ctx the parse tree
	 */
	void exitConsole(SymbMathParser.ConsoleContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#objeto}.
	 * @param ctx the parse tree
	 */
	void enterObjeto(SymbMathParser.ObjetoContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#objeto}.
	 * @param ctx the parse tree
	 */
	void exitObjeto(SymbMathParser.ObjetoContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDefinition(SymbMathParser.FunctionDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDefinition(SymbMathParser.FunctionDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#assign}.
	 * @param ctx the parse tree
	 */
	void enterAssign(SymbMathParser.AssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#assign}.
	 * @param ctx the parse tree
	 */
	void exitAssign(SymbMathParser.AssignContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#noEvalExpresion}.
	 * @param ctx the parse tree
	 */
	void enterNoEvalExpresion(SymbMathParser.NoEvalExpresionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#noEvalExpresion}.
	 * @param ctx the parse tree
	 */
	void exitNoEvalExpresion(SymbMathParser.NoEvalExpresionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresion(SymbMathParser.ExpresionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresion(SymbMathParser.ExpresionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#multiplication}.
	 * @param ctx the parse tree
	 */
	void enterMultiplication(SymbMathParser.MultiplicationContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#multiplication}.
	 * @param ctx the parse tree
	 */
	void exitMultiplication(SymbMathParser.MultiplicationContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#power}.
	 * @param ctx the parse tree
	 */
	void enterPower(SymbMathParser.PowerContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#power}.
	 * @param ctx the parse tree
	 */
	void exitPower(SymbMathParser.PowerContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#unary}.
	 * @param ctx the parse tree
	 */
	void enterUnary(SymbMathParser.UnaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#unary}.
	 * @param ctx the parse tree
	 */
	void exitUnary(SymbMathParser.UnaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAtom(SymbMathParser.AtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAtom(SymbMathParser.AtomContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#nNumber}.
	 * @param ctx the parse tree
	 */
	void enterNNumber(SymbMathParser.NNumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#nNumber}.
	 * @param ctx the parse tree
	 */
	void exitNNumber(SymbMathParser.NNumberContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#derivative}.
	 * @param ctx the parse tree
	 */
	void enterDerivative(SymbMathParser.DerivativeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#derivative}.
	 * @param ctx the parse tree
	 */
	void exitDerivative(SymbMathParser.DerivativeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#integral}.
	 * @param ctx the parse tree
	 */
	void enterIntegral(SymbMathParser.IntegralContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#integral}.
	 * @param ctx the parse tree
	 */
	void exitIntegral(SymbMathParser.IntegralContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#sumatoria}.
	 * @param ctx the parse tree
	 */
	void enterSumatoria(SymbMathParser.SumatoriaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#sumatoria}.
	 * @param ctx the parse tree
	 */
	void exitSumatoria(SymbMathParser.SumatoriaContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(SymbMathParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(SymbMathParser.FunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#functionCallParams}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCallParams(SymbMathParser.FunctionCallParamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#functionCallParams}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCallParams(SymbMathParser.FunctionCallParamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#vVector}.
	 * @param ctx the parse tree
	 */
	void enterVVector(SymbMathParser.VVectorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#vVector}.
	 * @param ctx the parse tree
	 */
	void exitVVector(SymbMathParser.VVectorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#matriz}.
	 * @param ctx the parse tree
	 */
	void enterMatriz(SymbMathParser.MatrizContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#matriz}.
	 * @param ctx the parse tree
	 */
	void exitMatriz(SymbMathParser.MatrizContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#lList}.
	 * @param ctx the parse tree
	 */
	void enterLList(SymbMathParser.LListContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#lList}.
	 * @param ctx the parse tree
	 */
	void exitLList(SymbMathParser.LListContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#labeledObject}.
	 * @param ctx the parse tree
	 */
	void enterLabeledObject(SymbMathParser.LabeledObjectContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#labeledObject}.
	 * @param ctx the parse tree
	 */
	void exitLabeledObject(SymbMathParser.LabeledObjectContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#magnitude}.
	 * @param ctx the parse tree
	 */
	void enterMagnitude(SymbMathParser.MagnitudeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#magnitude}.
	 * @param ctx the parse tree
	 */
	void exitMagnitude(SymbMathParser.MagnitudeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#uExpr}.
	 * @param ctx the parse tree
	 */
	void enterUExpr(SymbMathParser.UExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#uExpr}.
	 * @param ctx the parse tree
	 */
	void exitUExpr(SymbMathParser.UExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#uExprPower}.
	 * @param ctx the parse tree
	 */
	void enterUExprPower(SymbMathParser.UExprPowerContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#uExprPower}.
	 * @param ctx the parse tree
	 */
	void exitUExprPower(SymbMathParser.UExprPowerContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#seqOb}.
	 * @param ctx the parse tree
	 */
	void enterSeqOb(SymbMathParser.SeqObContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#seqOb}.
	 * @param ctx the parse tree
	 */
	void exitSeqOb(SymbMathParser.SeqObContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#sString}.
	 * @param ctx the parse tree
	 */
	void enterSString(SymbMathParser.SStringContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#sString}.
	 * @param ctx the parse tree
	 */
	void exitSString(SymbMathParser.SStringContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#parListIds}.
	 * @param ctx the parse tree
	 */
	void enterParListIds(SymbMathParser.ParListIdsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#parListIds}.
	 * @param ctx the parse tree
	 */
	void exitParListIds(SymbMathParser.ParListIdsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SymbMathParser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(SymbMathParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link SymbMathParser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(SymbMathParser.IdentifierContext ctx);
}